export class User {
  username: string;
  name: string;
  surname: string;
  email: string;
  role: string;
  registrationDate: Date;
  enabled: boolean;
}
