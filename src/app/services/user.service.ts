import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {User} from '../models/user';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class UserService {
  private readonly API_URL = 'http://localhost:8080/users';

  constructor(private httpClient: HttpClient) {
  }

  getAllUsers(): Observable<User[]> {
    return this.httpClient.get<User[]>(this.API_URL);
  }

  addUser(user: User): Observable<User> {
    return this.httpClient.post<User>(this.API_URL, user);
  }

  updateUser(user: User): Observable<User> {
    return this.httpClient.put<User>(this.API_URL + '/' + user.username, user);
  }

  deleteUser(username: string): Observable<Object> {
    return this.httpClient.delete(this.API_URL + '/' + username);
  }

}
