import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
// Import Containers

import {UsersComponent} from './views/users/users.component';

export const routes: Routes = [
  {
    path: '',
    component: UsersComponent,
    data: {
      title: 'User management'
    }
  },
  {
    path: '**', redirectTo: ''
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
