import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {UserService} from '../../services/user.service';
import {MatDialog, MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {User} from '../../models/user';
import {AddUserComponent} from './popups/add/add-user.component';
import {EditUserComponent} from './popups/edit/edit-user.component';
import {DeleteUserComponent} from './popups/delete/delete-user.component';

declare let jsPDF;

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  displayedColumns = ['username', 'name', 'surname', 'email', 'registrationDate', 'role', 'enabled', 'actions'];
  private userDataSource: MatTableDataSource<User> = new MatTableDataSource();

  constructor(private dialog: MatDialog,
              private userService: UserService,
              private snackBar: MatSnackBar) {
  }

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild('filterInput', {static: true}) filter: ElementRef;

  ngOnInit() {
    this.userService.getAllUsers().subscribe(users => {
      this.userDataSource.data = users;
      this.userDataSource.paginator = this.paginator;
      this.userDataSource.sort = this.sort;
    }, error => {
      console.log(error);
      this.snackBar.open(typeof error.error === 'string' ? error.error : error.message, 'Close');
    });
  }

  private refresh() {
    this.userService.getAllUsers().subscribe(users => {
      this.userDataSource.data = users;
    }, error => {
      this.snackBar.open(typeof error.error === 'string'  ? error.error : error.message, 'Close');
    });
  }

  private transformColumns() {
    return this.displayedColumns
      .filter(column => column !== 'actions')
      .map(column => {
        return {
          title: column.charAt(0).toUpperCase() + column.slice(1),
          dataKey: column
        };
      });
  }

  private exportPdf() {
    const doc = new jsPDF().autoTable(
      this.transformColumns(),
      this.userDataSource.filteredData,
      {
        didParseCell: (data) => {
          if (data.column.dataKey === 'registrationDate') {
            data.cell.text = data.cell.raw === 'RegistrationDate' ?
              'Registration Date' : new Date(data.cell.raw).toLocaleDateString();
          }
        }
      });
    doc.save('Users_' + new Date().toLocaleString() + '.pdf');
  }

  private addNewUser() {
    const dialogRef = this.dialog.open(AddUserComponent, {
      data: {user: new User()}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (!!result) {
        this.userDataSource.data.push(result);
        this.refresh();
        this.snackBar.open('User successfully added.', 'Close', {duration: 3000});
      }
    });
  }

  private editUser(username: string, name: string, surname: string, email: string, registrationDate: Date, role: string, enabled: boolean) {
    const dialogRef = this.dialog.open(EditUserComponent, {
      data: {username, name, surname, email, registrationDate, role, enabled}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (!!result) {
        const userIndex = this.userDataSource.data.findIndex(x => x.username === result.username);
        this.userDataSource.data[userIndex] = result.data;
        this.refresh();
        this.snackBar.open('User successfully edited.', 'Close', {duration: 3000});
      }
    });
  }

  private deleteUser(username: string, name: string, surname: string, email: string) {
    const dialogRef = this.dialog.open(DeleteUserComponent, {
      data: {username, name, surname, email}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (!!result) {
        const userIndex = this.userDataSource.data.findIndex(x => x.username === result.username);
        this.userDataSource.data.splice(userIndex, 1);
        this.refresh();
        this.snackBar.open('User successfully deleted.', 'Close', {duration: 3000});
      }
    });
  }

  private applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.userDataSource.filter = filterValue;
  }
}
