import {Component} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MatDialogRef, MatSnackBar} from '@angular/material';
import {UserService} from '../../../../services/user.service';
import {Roles} from '../../../../models/roles';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent {
  constructor(public dialogRef: MatDialogRef<AddUserComponent>,
              public dataService: UserService,
              private snackBar: MatSnackBar) {
  }
  keys = Object.keys;
  roles = Roles;
  formGroup = new FormGroup({
    username: new FormControl('', [Validators.required]),
    name: new FormControl('', [Validators.required]),
    surname: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required, Validators.email]),
    role: new FormControl('', [Validators.required]),
    enabled: new FormControl('', [])
  });

  private getErrorMessage(formControlName: string): string {
    const formControl = this.formGroup.get(formControlName);
    return formControl.hasError('required') ? 'Required field' :
      formControl.hasError('email') ? 'Not a valid email' : '';
  }

  private onCancel(): void {
    this.dialogRef.close();
  }

  private confirmAdd(): void {
    this.formGroup.value.registrationDate = new Date();
    this.dataService.addUser(this.formGroup.value).subscribe(user => {
      this.dialogRef.close(user);
    }, error => {
      this.snackBar.open(typeof error.error === 'string' ? error.error : error.message, 'Close');
    });
  }

}
