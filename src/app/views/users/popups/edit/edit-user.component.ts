import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatSnackBar} from '@angular/material';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../../../../services/user.service';
import {User} from '../../../../models/user';
import {Roles} from '../../../../models/roles';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent {

  constructor(public dialogRef: MatDialogRef<EditUserComponent>,
              @Inject(MAT_DIALOG_DATA) public user: User, public userService: UserService,
              private snackBar: MatSnackBar) {
  }

  keys = Object.keys;
  roles = Roles;
  formGroup = new FormGroup({
    username: new FormControl({value: this.user.username, disabled: true}, [Validators.required]),
    name: new FormControl(this.user.name, [Validators.required]),
    surname: new FormControl(this.user.surname, [Validators.required]),
    email: new FormControl(this.user.email, [Validators.required, Validators.email]),
    role: new FormControl(this.user.role, [Validators.required]),
    enabled: new FormControl(this.user.enabled, []),
    registrationDate: new FormControl(this.user.registrationDate, [])
  });

  private getErrorMessage(formControlName: string): string {
    const formControl = this.formGroup.get(formControlName);
    return formControl.hasError('required') ? 'Required field' :
      formControl.hasError('email') ? 'Not a valid email' : '';
  }

  private onNoClick(): void {
    this.dialogRef.close();
  }

  private stopEdit(): void {
    this.userService.updateUser(this.formGroup.getRawValue()).subscribe(user => {
      this.dialogRef.close(user);
    }, error => {
      this.snackBar.open(typeof error.error === 'string' ? error.error : error.message, 'Close');
    });
  }
}
