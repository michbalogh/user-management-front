import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatSnackBar} from '@angular/material';
import {UserService} from '../../../../services/user.service';
import {User} from '../../../../models/user';

@Component({
  selector: 'app-delete-user',
  templateUrl: './delete-user.component.html',
  styleUrls: ['./delete-user.component.css']
})
export class DeleteUserComponent {

  constructor(public dialogRef: MatDialogRef<DeleteUserComponent>,
              @Inject(MAT_DIALOG_DATA) public user: User, public userService: UserService,
              private snackBar: MatSnackBar) {
  }

  private onCancel(): void {
    this.dialogRef.close();
  }

  private confirmDelete(): void {
    this.userService.deleteUser(this.user.username).subscribe(() => {
      this.dialogRef.close(this.user);
    }, error => {
      this.snackBar.open(typeof error.error === 'string' ? error.error : error.message, 'Close');
    });
  }

}
